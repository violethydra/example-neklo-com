webpackJsonp([0],[
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($, jQuery, __webpack_provided_window_dot_jQuery) {

//============================
//    Name: index.js
//============================
// import for others scripts to use
window.$ = $;
__webpack_provided_window_dot_jQuery = jQuery; // import 'babel-polyfill';
// import 'owl.carousel';
// import 'jquery-scrollify/jquery.scrollify';

$(function () {
  /* ======= Global Actions ======= */
  var k = 'click',
      act = 'item-true',
      dis = 'item-false';
  /* ======= ---- ======= */

  /* ======= list ======= */

  var ___order__list_ = function ___order__list_() {
    pageMain.__load();
  };
  /* ======= ---- ======= */


  var pageMain = {
    __load: function __load() {
      var regexpEmail = /[\0-\x08\x0E-\x1F!-\x9F\xA1-\u167F\u1681-\u1FFF\u200B-\u2027\u202A-\u202E\u2030-\u205E\u2060-\u2FFF\u3001-\uFEFE\uFF00-\uFFFF]+@[\0-\x08\x0E-\x1F!-\x9F\xA1-\u167F\u1681-\u1FFF\u200B-\u2027\u202A-\u202E\u2030-\u205E\u2060-\u2FFF\u3001-\uFEFE\uFF00-\uFFFF]+\.[0-9A-Z_a-z]+$/gi,
          nothing = '';
      var setting = {
        // * Run functions once
        __firstRuns: function __firstRuns() {
          this.example();
        },
        // * Handlers
        __handlers: function __handlers() {
          var _this = this;

          $(window).on('scroll', function () {
            return _this.scrollUPShow();
          });
          $('.js__scrollUP').on(k, function () {
            return _this.scrollUPAnima();
          });
          $('.js__headerAccord .hdr__href-1').on(k, function (event) {
            return _this.headerAccord(event);
          });
        },

        /* ======= Code ======= */
        headerAccord: function headerAccord(event) {
          $(event.currentTarget).next().toggleClass(act);
        },
        example: function example() {
          console.log('HelloWorld');
        },
        scrollUPAnima: function scrollUPAnima() {
          $('html, body').animate({
            scrollTop: 0
          }, 500, 'swing');
        },
        scrollUPShow: function scrollUPShow() {
          $(window).scrollTop() >= 100 ? $('.js__scrollUP').removeClass(act + '-out') + $('.js__scrollUP').addClass(act) : $('.js__scrollUP').addClass(act + '-out') + $('.js__scrollUP').removeClass(act);
        },

        /* ======= ---- ======= */
        // * Loaders 
        __loaders: function __loaders() {
          this.__firstRuns();

          this.__handlers();
        }
      };

      setting.__loaders();
    }
  };
  /* ======= list ======= */

  ___order__list_();
  /* ======= ---- ======= */

});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0), __webpack_require__(0), __webpack_require__(0)))

/***/ })
],[1]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2xvYmFsLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvZ2xvYmFsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4vLyAgICBOYW1lOiBpbmRleC5qc1xyXG4vLz09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuXHJcbi8vIGltcG9ydCBmb3Igb3RoZXJzIHNjcmlwdHMgdG8gdXNlXHJcbndpbmRvdy4kID0gJDtcclxud2luZG93LmpRdWVyeSA9IGpRdWVyeTtcclxuXHJcbi8vIGltcG9ydCAnYmFiZWwtcG9seWZpbGwnO1xyXG4vLyBpbXBvcnQgJ293bC5jYXJvdXNlbCc7XHJcbi8vIGltcG9ydCAnanF1ZXJ5LXNjcm9sbGlmeS9qcXVlcnkuc2Nyb2xsaWZ5JztcclxuXHJcbiQoKCkgPT4ge1xyXG5cclxuXHQvKiA9PT09PT09IEdsb2JhbCBBY3Rpb25zID09PT09PT0gKi9cclxuXHRjb25zdFxyXG5cdFx0ayA9ICdjbGljaycsXHJcblx0XHRhY3QgPSAnaXRlbS10cnVlJyxcclxuXHRcdGRpcyA9ICdpdGVtLWZhbHNlJztcclxuXHQvKiA9PT09PT09IC0tLS0gPT09PT09PSAqL1xyXG5cclxuXHQvKiA9PT09PT09IGxpc3QgPT09PT09PSAqL1xyXG5cdGNvbnN0IF9fX29yZGVyX19saXN0XyA9ICgpID0+IHtcclxuXHRcdHBhZ2VNYWluLl9fbG9hZCgpO1xyXG5cdH07XHJcblx0LyogPT09PT09PSAtLS0tID09PT09PT0gKi9cclxuXHJcblx0Y29uc3QgcGFnZU1haW4gPSB7XHJcblx0XHRfX2xvYWQoKSB7XHJcblx0XHRcdGxldFxyXG5cdFx0XHRcdHJlZ2V4cEVtYWlsID0gL1xcUytbQF1cXFMrWy5dXFx3KyQvZ3NpLFxyXG5cdFx0XHRcdG5vdGhpbmcgPSAnJztcclxuXHJcblx0XHRcdGxldCBzZXR0aW5nID0ge1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdC8vICogUnVuIGZ1bmN0aW9ucyBvbmNlXHJcblx0XHRcdFx0X19maXJzdFJ1bnMoKSB7XHJcblx0XHRcdFx0XHR0aGlzLmV4YW1wbGUoKTtcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdC8vICogSGFuZGxlcnNcclxuXHRcdFx0XHRfX2hhbmRsZXJzKCkge1xyXG5cdFx0XHRcdFx0JCh3aW5kb3cpLm9uKCdzY3JvbGwnLCAoKSA9PiB0aGlzLnNjcm9sbFVQU2hvdygpKTtcclxuXHRcdFx0XHRcdCQoJy5qc19fc2Nyb2xsVVAnKS5vbihrLCAoKSA9PiB0aGlzLnNjcm9sbFVQQW5pbWEoKSk7XHJcblx0XHRcdFx0XHQkKCcuanNfX2hlYWRlckFjY29yZCAuaGRyX19ocmVmLTEnKS5vbihrLChldmVudCkgPT4gdGhpcy5oZWFkZXJBY2NvcmQoZXZlbnQpKTtcclxuXHRcdFx0XHR9LFxyXG5cclxuXHRcdFx0XHQvKiA9PT09PT09IENvZGUgPT09PT09PSAqL1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGhlYWRlckFjY29yZChldmVudCkge1xyXG5cdFx0XHRcdFx0JChldmVudC5jdXJyZW50VGFyZ2V0KS5uZXh0KCkudG9nZ2xlQ2xhc3MoYWN0KTtcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGV4YW1wbGUoKSB7XHJcblx0XHRcdFx0XHRjb25zb2xlLmxvZygnSGVsbG9Xb3JsZCcpO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0c2Nyb2xsVVBBbmltYSgpIHtcclxuXHRcdFx0XHRcdCQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHtzY3JvbGxUb3A6MH0sIDUwMCwgJ3N3aW5nJyk7XHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRzY3JvbGxVUFNob3coKSB7XHJcblx0XHRcdFx0XHQkKHdpbmRvdykuc2Nyb2xsVG9wKCkgPj0gMTAwXHJcblx0XHRcdFx0XHRcdD8gJCgnLmpzX19zY3JvbGxVUCcpLnJlbW92ZUNsYXNzKGFjdCArICctb3V0JykgKyAkKCcuanNfX3Njcm9sbFVQJykuYWRkQ2xhc3MoYWN0KVxyXG5cdFx0XHRcdFx0XHQ6ICQoJy5qc19fc2Nyb2xsVVAnKS5hZGRDbGFzcyhhY3QgKyAnLW91dCcpICsgJCgnLmpzX19zY3JvbGxVUCcpLnJlbW92ZUNsYXNzKGFjdCk7XHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHRcclxuXHRcdFx0XHQvKiA9PT09PT09IC0tLS0gPT09PT09PSAqL1xyXG5cclxuXHJcblxyXG5cclxuXHRcdFx0XHQvLyAqIExvYWRlcnMgXHJcblx0XHRcdFx0X19sb2FkZXJzKCkge1xyXG5cdFx0XHRcdFx0dGhpcy5fX2ZpcnN0UnVucygpO1xyXG5cdFx0XHRcdFx0dGhpcy5fX2hhbmRsZXJzKCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9O1xyXG5cdFx0XHRzZXR0aW5nLl9fbG9hZGVycygpO1xyXG5cdFx0fVxyXG5cdH07XHJcblx0LyogPT09PT09PSBsaXN0ID09PT09PT0gKi9cclxuXHRfX19vcmRlcl9fbGlzdF8oKTtcclxuXHQvKiA9PT09PT09IC0tLS0gPT09PT09PSAqL1xyXG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9nbG9iYWwuanMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQXpDQTtBQUNBO0FBMENBO0FBQ0E7QUFsREE7QUFvREE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=